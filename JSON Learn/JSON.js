

//====== JSON Files =====
// File type: ".json"
// MIME type: "application/json"


//====== data =====
/*
Data is in name/value pairs:
"firstName":"John"

Data is separated by commas:
"firstName":"John", "lastName":"Doe"

values
-number (integer or floating point)
-string (in double quotes)
-Boolean (true or false)
-array (in square brackets)
-object (in curly braces)
-null
*/

//===========================================================
//========================= objects ========================= 
//===========================================================
//Curly braces hold objects
var egObject = {"firstName":"John", "lastName":"Doe"}
var egFirstNameString = egObject.firstName;

//===========================================================
//========================= arrays ========================== 
//===========================================================
var egArray = [1,2,3,4]

// An Object holding an array
var egObjectWithArray = {"egArray": [1,2,3,4]};

// An Array of Objects
var egArrayOfObjects = [ {"firstName":"John", "lastName":"Doe"}, {"firstName":"Anna", "lastName":"Smith"}, {"firstName":"Peter", "lastName":"Jones"} ]

//===========================================================
//=======================  JSON.parse =======================
//===========================================================
// A JSON parser will recognize only JSON text and will not compile scripts

// Create JSON data as Text
var jsonData = '{"firstName":"John" , "lastName":"Doe"}'

// parse JSON data to JS Object
var obj = JSON.parse(jsonData);

// Use object
var firstName = obj.firstName;


//===== eval() to Parse 
// Older browsers without the support for the JavaScript function JSON.parse() can use eval()
// The eval() function can compile and execute any JavaScript. This represents a potential security problem. Try to avoid it.
// JSON.parse does not run js scripts and it is faster
// For older browsers, a JavaScript library is available at https://github.com/douglascrockford/JSON-js

// Works exactly the same except JSON.parse is replaced with eval and extra text brackets ()
// parse JSON data to JS Object using eval()
var obj = eval ("(" + jsonData + ")");
						  