$(document).ready(function () {

  var Cropper = window.Cropper;
  var URL = window.URL || window.webkitURL;

   var image = document.getElementById('cropping-image');

  var options = {
    aspectRatio: 1 / 1,
    preview: '.img-preview',
    viewMode: 0,
    dragMode: 'crop',
    autoCropArea: 0.8,
    restore: true,
    modal: true,
    guides: true,
    highlight: true,
    cropBoxMovable: true,
    cropBoxResizable: true,
    ready: function(e) {   
    },
    cropstart: function(e) {    
    },
    cropmove: function(e) { 
    },
    cropend: function(e) {      
    },
    crop: function(e) {
         },
    zoom: function(e) {   
    }
  };
  var cropper = new Cropper(image, options);

  var uploadedImageType = 'image/jpeg';
  var uploadedImageURL;

  // Tooltip
  $('[data-toggle="tooltip"]').tooltip();


  // Import image
  var inputImage = document.getElementById('inputImage');

  if (URL) {
    inputImage.onchange = function() {
      var files = this.files;
      var file;

      if (cropper && files && files.length) {
        file = files[0];

        if (/^image\/\w+/.test(file.type)) {
          uploadedImageType = file.type;

          if (uploadedImageURL) {
            URL.revokeObjectURL(uploadedImageURL);
          }

          image.src = uploadedImageURL = URL.createObjectURL(file);
          cropper.destroy();
          cropper = new Cropper(image, options);
          inputImage.value = null;
        } else {
          window.alert('Please choose an image file.');
        }
      }
    };
  } else {
    inputImage.disabled = true;
    inputImage.parentNode.className += ' disabled';
  }







  document.body.onkeydown = function(event) {
    var e = event || window.event;

    if (!cropper || this.scrollTop > 300) {
      return;
    }

    switch (e.keyCode) {
      case 37:
        e.preventDefault();
        cropper.move(-1, 0);
        break;

      case 38:
        e.preventDefault();
        cropper.move(0, -1);
        break;

      case 39:
        e.preventDefault();
        cropper.move(1, 0);
        break;

      case 40:
        e.preventDefault();
        cropper.move(0, 1);
        break;
    }
  };

  $("#mode-move").click(function(event) {
    cropper.clear();
    var result = cropper['setDragMode']('move');
    cropper.crop();
  });
  
  $("#mode-crop").click(function(event) {
    cropper.clear();
    var result = cropper['setDragMode']('crop');
    cropper.crop();
  });

  $("#zoom-in").click(function(event) {
    cropper.clear();
    var result = cropper['zoom']('0.1');
    cropper.crop();
  });
  
  $("#zoom-out").click(function(event) {
    cropper.clear();
    var result = cropper['zoom']('-0.1');
    cropper.crop();
  });

$("#rotate-left").click(function(event) {
  cropper.clear();
  var result = cropper['rotate']('-15');
  cropper.crop();
});

$("#rotate-right").click(function(event) {
  cropper.clear();
  var result = cropper['rotate']('15');
  cropper.crop();
});

$("#move-left").click(function(event) {
  cropper.clear();
  var result = cropper['move']('-10', '0');
  cropper.crop();
});

$("#move-right").click(function(event) {
  cropper.clear();
  var result = cropper['move']('10',0);
  cropper.crop();
});

$("#move-up").click(function(event) {
  cropper.clear();
  var result = cropper['move']('0', '-10');
  cropper.crop();
});

$("#move-down").click(function(event) {
  cropper.clear();
  var result = cropper['move']('0', '10');
  cropper.crop();
});

var isFlippedHorizontal;
$("#flip-horizontal").click(function(event) { 
  var scaleValue= "-1";
  if (isFlippedHorizontal) {
    scaleValue = "1";
  }
  isFlippedHorizontal = !isFlippedHorizontal;

  cropper.clear();
  var result = cropper['scaleX'](scaleValue);
  cropper.crop();
});

var isFlippedVertical;
$("#flip-vertical").click(function(event) {
  
var scaleValue= "-1";

if (isFlippedVertical) {
  scaleValue = "1";
} 
isFlippedVertical = !isFlippedVertical;

  cropper.clear();
  var result = cropper['scaleY'](scaleValue);
  cropper.crop();
});

$("#getCrop").click(function(event) {
  var option = {
    "width": 300,
    "height": 300,
    "fillColor": '#ff0000'
  };
   
    var result = cropper['getCroppedCanvas'](option);  
    if (result) {
      $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);

      var download = document.getElementById('download-anchor');   

      if (download.download === undefined) {
        download.download = "crop.jpg";
      }

      var dataUrl = result.toDataURL(uploadedImageType);
      download.href = dataUrl;

	  $('#upload-anchor').off('click');
	  
      $("#upload-anchor").click(function(event) {
       
        var data = new FormData();
        var blob54321 = dataURItoBlob(dataUrl);
        data.append('image', blob54321);

        $.ajax({
            url: 'https://localhost:44307/api/v1/MyUserProfile/UploadProfilePic',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            method: 'POST',
            headers: {
                'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJhYTc4OTY5Ny0yNjljLTQ1NzUtODRkNy1iY2E5MzQxMTY5MTEiLCJleHAiOjE1NTQxNDQwMjIsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDk5ODMiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQ5OTgzIn0.vLi-pc2pEJamp807vWLpOvhKRlZAuowbc6s8ioU4m8E'
            },
            success: function (data, status, jqXHR) {
                console.log("success");
            },
            error: function (jqXHR, status, err) {          
              console.log(err);
            },
            complete: function (jqXHR, status) {
                  console.log(jqXHR);
            }
        });

      });
    
    }
    cropper.crop();
  });
 
});


function dataURItoBlob(dataURI) {
  var binary = atob(dataURI.split(',')[1]);
  var array = [];
  for (var i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
  }
  return new Blob([new Uint8Array(array)], { type: 'image/jpeg' });
}
