
//============================= Maths ===========================
var maxNum = Math.max(10,5,2);
var minNum = Math.min(10,5,2);
var absNum = Math.abs(-5); // absolute
var powNum = Math.pow(5,2); // power of
var sqrtNum = Math.sqrt(9); // square root
var cbrtNum = Math.cbrt(8); // cube root

//===== Geometry / Trig
var piNum = Math.PI; // 3.14159265358
var sinNum = Math.sin(90);
var cosNum = Math.cos(90);
var tabNum = Math.tan(90)

//===== Rounding
var ceilNum = Math.ceil(6.45); // round up
var floorNum = Math.floor(6.45); // round down
var roundNum = Math.round(6.45); // round to nearest

//===== Logs
var logNum = Math.log(100); // natural log
var log2Num = Math.log2(4); // base 2 log
var log10Num = Math.log10(100); // base 10 log
function getBaseLog(logBase, y) 
{
  return Math.log(y) / Math.log(logBase);
}

//===== Random Numbers
var randNum = Math.random(0);

function getRandomArbitrary(min, max) 
{
  return Math.floor(Math.random() * (max - min)) + min;
}

function getRandomInt(min, max) 
{
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

//===== Math Vs Concatination
// Commas seperate math functionality from string concatination
document.write("5+4=",5+4,"<br/>"); // adds numbers
document.write("5+4="+5+4+"<br/>"); // concatinates numbers as strings

//===== Increment/Decrement
var num = 5;
document.write(num++,"<br/>"); // prints then increments
document.write(++num,"<br/>"); // increments then prints
document.write(num--,"<br/>"); // prints then decrements
document.write(--num,"<br/>"); // decrements then prints

//===== Set decimal places
// .toFixed() allows you to set the decimal places of a number
var num = 12.5555.toFixed(2);

