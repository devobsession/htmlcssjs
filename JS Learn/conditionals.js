
// ===== If statement
if(1 === 2)
{}
else if(1 === 1)
{}
else
{}

// Relational Operators

if(1 == "1") // verifies that the values are equal
{}

if(1 === "1") // verifies that the values and types are equal
{}

if(1 != 2)
{}

if(1 > 2)
{}

if(1 >= 2)
{}

if(1 < 2)
{}

if(1 <= 2)
{}

// Logical Operators

if(true && false)
{}

if(true || false)
{}


// ===== Switch statement
var toSwitch = 2;

switch(toSwitch)
{
	case 1 :
		document.write("1");
		break;
	case 2 :
	case 3 :	
		document.write("2 or 3");
		break;
	default:
		document.write("anything else");
		break;	
}

// ===== Inline If statement
var inlineIfResult = (a > b) ? "true" : "false";


//============================= Loops ===========================

// while loop
var whileI =0;
while(whileI < 10)
{
	whileI++;
}

// do while loop
var doWhileI =0;
do
{
	doWhileI++;	
}while(doWhileI < 10)

// for loop
for(i=0;i<10; i++)
{	
}

// foreach loop
var customer = {name: "bob", number: "10", address: "123 street"};
for(val in customer)
{
	var getValue = customer[k];	
}

// Continue / Break
for(i=0;i<10; i++)
{	
if(i < 5)
{
	continue; // skip this iteration
}
break; // break out of loop
}
