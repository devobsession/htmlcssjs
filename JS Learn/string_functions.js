
//============================= String ===========================

var concat = "bla"+ "bla" +"bla";
var escape = "bla \"bla\" bla";

var egString = "gerbger eg trgb trgbtdrb sf";
var stringLength = egString.length;
var index = egString.indexOf("eg");
var slice1 = egString.slice(2,5); // get sub string from index 2 to 5
var slice2 = egString.slice(5); // get sub string from index 5 to end
var substring = egString.substr(5,4); //// get sub string from index 5 to +4(9)

var replace = egString.replace("eg", "bla"); // replace "eg" with "bla"
var charIndex = egString.charAt(2); // get the character at index 2
var stringArray = egString.split(" "); // split the string by " "
var trimEnds = egString.trim(); // remove white space from edges
var egStringInUpper = egString.toUpperCase();
var egStringLower = egString.toLowerCase();

//===== Style
var styleString = "style this string with extension methods";
var styleStringBig = styleString.big();
var styleStringBold = styleString.bold();
var styleStringFontcolor = styleString.fontcolor("blue");
var styleStringFontsize = styleString.fontsize("8em");
var styleStringItalics = styleString.italics();
var styleStringLink = styleString.link("https://www.youtube.com/");
var styleStringSmall = styleString.small();
var styleStringStrike = styleString.strike();
var styleStringSub = styleString.sub();
var styleStringSup = styleString.sup();
