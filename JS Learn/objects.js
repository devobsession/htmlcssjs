
//=========================  Objects  ==========================

// Create an Object
var employee = {"firstName":"John", "lastName":"Doe"};

// Access an object
var firstName = employee.firstName;

// Modify an Object
employee.firstName = "New Name";

//=========================  Array of Objects  ==========================

// Create an array of objects
var employees = [
    {"firstName":"John", "lastName":"Doe"}, 
    {"firstName":"Anna", "lastName":"Smith"}, 
    {"firstName":"Peter", "lastName": "Jones"}
];

// Access an object from the array
var firstName = employees[0].firstName;

// Modify an Object from the array
employees[0].firstName = "New Name";