
// https://developer.mozilla.org/en/docs/Web/API/XMLHttpRequest/Using_XMLHttpRequest

//===== Create Http Request Object
var xmlhttp = new XMLHttpRequest();


//===== Set a function to execute for when the call completes
xmlhttp.onreadystatechange = function() 
{
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) 
	{
        var response = JSON.parse(xmlhttp.responseText);
		var name = response.name;
		
		// Set text to element on page
		document.getElementById("demo").innerHTML = name;
		
		//==== Add the below Html to the body of a page which is in this folder
		//<p id="demo"></p> 
		//<script src="JS_API_Call.js"></script>
	}
}

//===== Set the request 
xmlhttp.open("GET", "http://swapi.co/api/people/1/", true);
// 1. Http Method/ Action
// 2. The Url to make the request to 
// 3. bool for asynchronousity (true = asynchronous) (some browsers have depracated synchronous requests)

//===== Send the request
xmlhttp.send();
