

// Create Arrays
var egArray = ["a","b","c"];
var egArrayNums = [2,5,1,6,3];

// get value
var getValue = egArray[2];

// add value
egArray[3] = "d"; // use the next avaialbe index
egArray.splice(2,0,"xx","yy", "zz"); // starts from index 2, replaces the next 0 elements, then adds the listed elements
egArray.push("e"); // add to end of array
egArray.unshift("x"); // add to begining of array

// set value
egArray[3] = "e";
egArray.splice(2,1,"replaced"); // starts from index 2, replaces the next 1 element, then adds the element
egArray.splice(2,1,"replaced", "added"); // starts from index 2, replaces the next 1 element, then adds the listed elements

// remove value
egArray.splice(2,2); // starts from index 2, removes the next 2 elements
delete egArray[3]; // deletes the value but keeps the space so index can still be used
egArray.pop(); // removes last element
egArray.shift(); // removes first element

// Combine to arrays
var combined numbers = egArray.concat(egArrayNums);

// Sort
egArray.sort(); // sort alphbetical acending
egArray.reverse(); // sort alphbetical decending
egArrayNums.sort(function(x,y){return x-y}); // sort numbers acending
egArrayNums.sort(function(x,y){return y-x}); // sort numbers decending

// Tostring
egArray.valueOf();
egArray.join(", ");

// Length
var arrayLength = egArray.length;
