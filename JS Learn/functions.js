
function egFunction(egParam)
{
	var localVariable = "this can not be accessed outside this function";
		return egParam;
}

//===== pass functions into functions =====
function passInFunc(egFunc) // takes a function as a parameter
{
	var x = egFunc(1); // can use the funciton passed in
}

passInFunc(egFunction); // call your function and pass in the name of a function

//===== store functions =====
var egStoredFunction = function(num){return num;} // store function
egStoredFunction(2); // use stored function


//===== multiple unkown arguments =====
function sumArgs()
{
	var sum = 0;	
	for(i = 0; i< arguments.length; i++) // access arguments with "arguments" (an array)
	{
		sum += arguments[i];		
	}
	return sum;	
}

sumArgs(1,2,3,4,5,6); // add any amount of arguments regardless of the functions parameters



